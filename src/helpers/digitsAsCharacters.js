/* eslint-disable prettier/prettier */
export const zero = 
  ' _ ' +
  '| |' +
  '|_|';

export const one = 
  '   ' +
  '  |' +
  '  |';

export const two =
  ' _ ' +
  ' _|' +
  '|_ ';

export const three =
  ' _ ' +
  ' _|' +
  ' _|';

export const four =
  '   ' +
  '|_|' +
  '  |';

export const five =
  ' _ ' +
  '|_ ' +
  ' _|';

export const six =
  ' _ ' +
  '|_ ' +
  '|_|';

export const seven =
  ' _ ' +
  '  |' +
  '  |';

export const eight =
  ' _ ' +
  '|_|' +
  '|_|';

export const nine =
  ' _ ' +
  '|_|' +
  ' _|';