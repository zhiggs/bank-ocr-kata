// The pipes, underscores, and spaces that compose each digit are stored
// here as bits. The bits are determined by representing each character
// within the digit, from top right to bottom left, as a 0 for an empty
// space, and a 1 for a | or _. For example:
//  _      010
// |_  --> 110 --> 010110011
//  _|     011
// This allows us to use bitwise operators and logical masks to easily toggle
// individual characters when checking for read errors (Or would have, if I had made it that far).
const hexOcrDigits = new Map();
hexOcrDigits.set(0, 0x0af);
hexOcrDigits.set(1, 0x009);
hexOcrDigits.set(2, 0x09e);
hexOcrDigits.set(3, 0x09b);
hexOcrDigits.set(4, 0x039);
hexOcrDigits.set(5, 0x0b3);
hexOcrDigits.set(6, 0x0b7);
hexOcrDigits.set(7, 0x089);
hexOcrDigits.set(8, 0x0bf);
hexOcrDigits.set(9, 0x0bb);

// Creates a numeric representation of the characters, where an empty space is
// represented as 0, and a pipe or underscore is represented as 1
const stringToNumber = (digitLine) => {
  const chars = digitLine.split('');
  const byteArray = chars.map((char) => (char === ' ' ? 0 : 1));
  return parseInt(byteArray.join(''), 2);
};

// Uses bitwise shifting to combine all three lines of the character
const mergeNumericArray = (byteArray) =>
  (byteArray[0] << 6) + (byteArray[1] << 3) + byteArray[2];

/**
 * Takes the characters for a single digit as read by the OCR and determines its decimal representation
 * @param {string} digit The digit as read by the OCR
 * @returns The decimal representation of the digit, or -1 if the digit was not recognized
 */
export const readDigit = (digit) => {
  const linesAsNumbers = digit.match(/.{1,3}/g).map(stringToNumber);
  const mergedNumbers = mergeNumericArray(linesAsNumbers);

  let scannedDigit = -1;
  hexOcrDigits.forEach((hex, decimal) => {
    if (hex === mergedNumbers) {
      scannedDigit = decimal;
    }
  });

  return scannedDigit;
};
