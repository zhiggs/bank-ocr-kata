import { readDigit } from './digitReader.js';

/**
 * A recursive method to read each digit of an account number and return as a complete string
 * @param {string} inputString The complete account number string read from the input file
 * @returns {string} The account number as a string of digits, with unknown digits represented as question marks
 */
export const readAccountNumber = (inputString) => {
  const charArray = inputString.split('');
  const remainingLineLength = charArray.length / 3;

  const lineThree = charArray.splice(remainingLineLength * 2, 3);
  const lineTwo = charArray.splice(remainingLineLength, 3);
  const lineOne = charArray.splice(0, 3);

  const firstDigitString = [...lineOne, ...lineTwo, ...lineThree].join('');
  const firstDigit = readDigit(firstDigitString);

  const remainingDigits =
    charArray.length < 3 ? '' : readAccountNumber(charArray.join(''));

  return `${firstDigit === -1 ? '?' : firstDigit}${remainingDigits}`;
};

/**
 * Validates the account number using the following calculation:
 * account number:  3  4  5  8  8  2  8  6  5
 * position names:  d9 d8 d7 d6 d5 d4 d3 d2 d1
 * (d1 + 2*d2 + 3*d3 +..+ 9*d9) mod 11 = 0
 * @param {string} accountNumber The account number as recognized by OCR
 * @returns {boolean} The validity of the given account number
 */
export const validateAccountNumber = (accountNumber) => {
  let checksum = accountNumber.charAt(8);

  for (let x = 1; x < 9; x++) {
    const charIndex = 8 - x;
    checksum += accountNumber.charAt(charIndex) * x;
  }

  return checksum % 11 === 0;
};
