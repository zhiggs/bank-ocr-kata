import * as digits from '../helpers/digitsAsCharacters.js';
import { readDigit } from '../digitReader.js';

describe('readDigit', () => {
  it('reads 0', () => {
    const { zero } = digits;

    const digit = readDigit(zero);
    expect(digit).toEqual(0);
  });

  it('reads 1', () => {
    const { one } = digits;

    const digit = readDigit(one);
    expect(digit).toEqual(1);
  });

  it('reads 2', () => {
    const { two } = digits;

    const digit = readDigit(two);
    expect(digit).toEqual(2);
  });

  it('reads 3', () => {
    const { three } = digits;

    const digit = readDigit(three);
    expect(digit).toEqual(3);
  });

  it('reads 4', () => {
    const { four } = digits;

    const digit = readDigit(four);
    expect(digit).toEqual(4);
  });

  it('reads 5', () => {
    const { five } = digits;

    const digit = readDigit(five);
    expect(digit).toEqual(5);
  });

  it('reads 6', () => {
    const { six } = digits;

    const digit = readDigit(six);
    expect(digit).toEqual(6);
  });

  it('reads 7', () => {
    const { seven } = digits;

    const digit = readDigit(seven);
    expect(digit).toEqual(7);
  });

  it('reads 8', () => {
    const { eight } = digits;

    const digit = readDigit(eight);
    expect(digit).toEqual(8);
  });

  it('reads 9', () => {
    const { nine } = digits;

    const digit = readDigit(nine);
    expect(digit).toEqual(9);
  });
});
