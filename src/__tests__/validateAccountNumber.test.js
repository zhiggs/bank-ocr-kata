import { validateAccountNumber } from '../accountNumberReader.js';

describe('validateAccountNumber', () => {
  it('validates a valid number', () => {
    const isValid = validateAccountNumber('457508000');
    expect(isValid).toEqual(true);
  });

  it('rejects an invalid number', () => {
    const isValid = validateAccountNumber('664371495');
    expect(isValid).toEqual(false);
  });
});
