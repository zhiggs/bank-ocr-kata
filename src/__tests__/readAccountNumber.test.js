import * as accountNumbers from '../helpers/accountNumbersAsCharacters.js';
import { readAccountNumber } from '../accountNumberReader.js';

describe('accountNumberReader', () => {
  it('reads zeros', () => {
    const { zeros } = accountNumbers;

    const accountNumber = readAccountNumber(zeros);
    expect(accountNumber).toEqual('000000000');
  });

  it('reads ones', () => {
    const { ones } = accountNumbers;

    const accountNumber = readAccountNumber(ones);
    expect(accountNumber).toEqual('111111111');
  });

  it('reads twos', () => {
    const { twos } = accountNumbers;

    const accountNumber = readAccountNumber(twos);
    expect(accountNumber).toEqual('222222222');
  });

  it('reads threes', () => {
    const { threes } = accountNumbers;

    const accountNumber = readAccountNumber(threes);
    expect(accountNumber).toEqual('333333333');
  });

  it('reads fours', () => {
    const { fours } = accountNumbers;

    const accountNumber = readAccountNumber(fours);
    expect(accountNumber).toEqual('444444444');
  });

  it('reads fives', () => {
    const { fives } = accountNumbers;

    const accountNumber = readAccountNumber(fives);
    expect(accountNumber).toEqual('555555555');
  });

  it('reads sixes', () => {
    const { sixes } = accountNumbers;

    const accountNumber = readAccountNumber(sixes);
    expect(accountNumber).toEqual('666666666');
  });

  it('reads sevens', () => {
    const { sevens } = accountNumbers;

    const accountNumber = readAccountNumber(sevens);
    expect(accountNumber).toEqual('777777777');
  });

  it('reads eights', () => {
    const { eights } = accountNumbers;

    const accountNumber = readAccountNumber(eights);
    expect(accountNumber).toEqual('888888888');
  });

  it('reads nines', () => {
    const { nines } = accountNumbers;

    const accountNumber = readAccountNumber(nines);
    expect(accountNumber).toEqual('999999999');
  });

  it('reads oneThroughNine', () => {
    const { oneThroughNine } = accountNumbers;

    const accountNumber = readAccountNumber(oneThroughNine);
    expect(accountNumber).toEqual('123456789');
  });

  it('reads oneThroughNine', () => {
    const { oneThroughNine } = accountNumbers;

    const accountNumber = readAccountNumber(oneThroughNine);
    expect(accountNumber).toEqual('123456789');
  });

  it('reads ninesWithQuestion', () => {
    const { ninesAndQuestion } = accountNumbers;

    const accountNumber = readAccountNumber(ninesAndQuestion);
    expect(accountNumber).toEqual('99999999?');
  });
});
