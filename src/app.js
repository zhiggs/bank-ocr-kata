import { file } from './mockInputFile.js';
import {
  readAccountNumber,
  validateAccountNumber,
} from './accountNumberReader.js';

// Mock the reading of an OCR file
const lines = file.split('\n');
const outputLines = [];

// Read and validate each account number
while (lines.length > 0) {
  const accountNumberString = lines.splice(0, 3).join('');
  lines.splice(0, 1); // Remove the empty line
  const accountNumber = readAccountNumber(accountNumberString);

  if (accountNumber.indexOf('?') > -1) {
    outputLines.push(`${accountNumber} ILL`);
    continue;
  }

  if (!validateAccountNumber(accountNumber)) {
    outputLines.push(`${accountNumber} ERR`);
    continue;
  }

  outputLines.push(`${accountNumber}`);
}

// Mock the output of the results by printing to the console
outputLines.forEach((line) => console.log(line));
